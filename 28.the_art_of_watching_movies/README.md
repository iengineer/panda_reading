# 本书导语

## 看电影的艺术

[美] 约瑟夫·M. 博格斯 / 丹尼斯·W. 皮特里

### 内容简介

《城市意象》这本书讲述的内容有关城市的面貌，以及它的重要性和可变性。城市的景观在城市的众多角色中，同样是人们可见、可忆、可喜的源泉。赋予城市视觉形态是一种特殊而且相当新的设计问题。

- 内容简介

《城市意象》这本书讲述的内容有关城市的面貌，以及它的重要性和可变性。城市的景观在城市的众多角色中，同样是人们可见、可忆、可喜的源泉。赋予城市视觉形态是一种特殊而且相当新的设计问题。

- 编辑推荐

一座城市，无论景象多么普通都可以带来欢乐。从《城市意象》这本书中我们发现——城市如同建筑，是一种空间的结构，只是尺度更巨大，需要用更长的时间过程去感知。城市设计可以说是一种时间的艺术，然而它与别的时间艺术，比如已掌握的音乐规律完全不同。很显然，不同的条件下，对于不同的人群，城市设计的规律有可能被倒置、打断、甚至是彻底废弃。 [2] 

- 作者简介

凯文·林奇，任教于麻省理工学院建筑学院三十年之久，他帮助建立了城市规划系，并将之发展成为世界上最著名的建筑学院之一。1988年，他的家人，朋友，同事为了纪念他，以他的名义设立了凯文·林奇奖学金，用以奖掖后进和资助建筑学院的图书馆。

---

整理 BY iEngineer 仅供内部参考
