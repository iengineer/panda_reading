# 今日总结

关键词：

实验，随机，伦理

要点：

1. 通常科学研究的第一步是观察，第二步是推测原因，第三步则是提出一个可以验证的假设，最后再设计一个实验去证实或者证伪这个假设。
2. 实验环境最重要的优点是，实验者可以随机将参与者分配到不同的实验条件下进行观察，保证实验的严谨。
3. 心理学实验面临的挑战主要有：如何把握控制和影响实验参与者的分寸，如何才能让实验更具现实性，有时需对参与者隐瞒真实情况。
4. 实验者都应该谨慎地面对伦理问题，努力避开那些会导致参与者不愉快的程序，向参与者强调他们拥有随时退出实验的自由，在涉及隐瞒等不安因素的实验结束后，详细地向参与者解释缘由，想方设法地使他们在离开实验室时如同来时一样身心状态良好。

思考与讨论：

《社会性动物》全书就到这里结束了，我们用两个星期的时间谈论了社会上、生活中许多重要的现象，包括从众、宣传与说服、攻击性、偏见、喜欢与爱等等。我们可以看到，心理学是一个十分有趣，与我们的日常生活极其贴近的学科，但阅读的过程中难免有些晦涩无聊的地方，这是因为我们需要将观察到的现象抽象总结成为可以解释大多数类似情况的理论。谢谢大家的认真与耐心，很荣幸和大家一起读完这本社会心理学的经典著作，也欢迎大家从这本书开始走进心理学的世界。