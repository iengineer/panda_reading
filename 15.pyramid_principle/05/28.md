# 28 在PPT中呈现金字塔结构（一）

我们首先要明确的是，PPT的主要目的是向观众展示你的观点，因此我们要遵守一些基本准则：

1. 文字幻灯片要尽量简洁，只写最重要的总结性文字、提炼概括文字的思想。
2. PPT要图文并茂，文字、图表相互配合。因为PPT分为文字和图片两类，所以理想的搭配比例是图表90%，文字10%。文字的作用是说明整个展示的框架，强调重要的思想、观点、论点和措施。图表的作用是用来呈现文字难以表达的数据和结构关系。
3. PPT要呈现经深思熟虑之后的故事梗概和剧本。

![深思熟虑](../_imager/052801.jpg)

对于PPT，其实我们要做好的只有三方面：设计文字PPT、图表PPT和故事梗概。

(一)设计文字PPT

文字PPT和你的讲稿是有很大差别的，在PPT中尽量只呈现金字塔结构中的重要论点。

要记住一些指导原则：

1. 每张PPT只说明和展示一个论点。
2. 论点使用完整的陈述句，而不是关键词。
3. 文字尽可能简短。
4. 尽可能使用数字和简单词汇。
5. 字号要大。
6. 注重幻灯片的趣味性（布局、字号和颜色）。
7. 逐级展开呈现，提高趣味性。

这些原则看起来像是一些正确的废话，但在实际运用中，很多人往往做得非常不好。

首先，PPT的设计问题，网上已经有非常多的模板可以选择，不过如果你的PPT风格还是被人吐槽很丑的话，这其实是审美问题，这个问题太大，也不是本书主要解决的，如果你决心要提高审美能力的话，笔者推荐多阅读艺术、设计类的书籍。

其次，也是最重要的问题，在于许多人的归纳总结或者说阅读理解能力是不够的。我们在应试教育阶段对语文的学习是不足的，最大的问题是阅读量非常低，除了课本上的十几篇文章，相信很少人会另外花时间去读书，这就导致许多人的阅读水平停留在初中阶段，遇到稍微有难度的阅读就异常痛苦。自然地，如果你没有通过阅读和学习知道某件事可以这样表达，那当你要表达时，你就会发现自己有时讲半天都讲不明白，但别人三两句就讲清楚了。