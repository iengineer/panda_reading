# Summary

* [本书导语](README.md)

## 1  中国哲学的背景和儒、墨学说
* [1 中国哲学的精神](01.md#中国哲学的精神)
* [2 中国哲学的背景](01.md#中国哲学的背景)
* [3 诸子的由来](01.md#诸子的由来)
* [4 孔子：第一位教师](01.md#孔子：第一位教师)
* [5 墨子：孔子的第一位反对者](01.md#墨子：孔子的第一位反对者)

## 2 先秦诸子流派（一）
* [6 道家的第一阶段：杨朱](02.md#道家的第一阶段：杨朱)
* [7 儒家的理想主义流派：孟子](02.md#儒家的理想主义流派：孟子)
* [8 名家](02.md#名家)
* [9 道家的第二阶段：老子](02.md#道家的第二阶段：老子)
* [10 道家的第三阶段：庄子](02.md#道家的第三阶段：庄子)

## 3 先秦诸子流派（二）
* [11 后期的墨家](03.md#后期的墨家)
* [12 阴阳家和中国早期的宇宙发生论](03.md#阴阳家和中国早期的宇宙发生论)
* [13 儒家的现实主义流派：荀子](03.md#儒家的现实主义流派：荀子)
* [14 韩非子与法家](03.md#韩非子与法家)
* [15 儒家的形而上学](03.md#儒家的形而上学)
* [16 治国平天下的哲学主张](03.md#治国平天下的哲学主张)

## 4 汉唐的思想哲学主流
* [17 汉帝国的理论家：董仲舒](04.md#汉帝国的理论家：董仲舒)
* [18 儒家兴盛和道家再起](04.md#儒家兴盛和道家再起)
* [19 新道学：崇尚理性的玄学](04.md#新道学：崇尚理性的玄学)
* [20 新道家：豁达率性的风格](04.md#新道家：豁达率性的风格)
* [21 中国佛学的基础](04.md#中国佛学的基础)
* [22 禅宗：静默的哲学](04.md#禅宗：静默的哲学)

## 5 宋代以来的新儒学
* [23 新儒家：宇宙论者](05.md#新儒家：宇宙论者)
* [24 新儒家：两个学派的开端](05.md#新儒家：两个学派的开端)
* [25 新儒家：主张柏拉图式理念的理学](05.md#新儒家：主张柏拉图式理念的理学)
* [26 新儒学中的另一派：宇宙心学](05.md#新儒学中的另一派：宇宙心学)
* [27 西方哲学的传入](05.md#西方哲学的传入)
* [28 厕身现代世界的中国哲学](05.md#厕身现代世界的中国哲学)

