# 31 “觅母”的本质也是复制

如果把人类文化看作生物圈，那么“觅母”（Meme，又译模因、拟子等）就是文化的基因。

文化的传播与基因十分相似，但演化的速度却要快速许多。语言、音乐、流行服饰、建筑风格等等，一切上层建筑都在演化。它们可能是与基因有所关联的，但也可能与基因毫无关系。正因此，试图直接用基因进化论来解释人类社会与文化，是行不通的。

但是无论形式如何，“觅母”与基因有一个共同的本质，那就是复制。所有生命都是由“复制”这一本质现象繁衍和进化而来的，DNA是地球生物圈中最成功的复制者，但没有理由认为这种双螺旋的结构就是唯一的。任何时候，若有某些自然条件导致新的复制者兴起，它就能复制自己；总有一天这种新的复制者会接管整个世界，并开始它自身的崭新的进化；这崭新的进化一旦开始了，它就不会再屈服于旧的复制者的控制——一如当今复杂的DNA淘汰了“原始营养汤”中简单的复制基因一样。